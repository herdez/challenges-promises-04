/*
Given the following code:


function job(test) {
    if (test) {
        return aNewPromise();
    } else {
        return 42;
    }
}

var value = NaN

var result = job(value);

if (typeof result === 'object' && typeof result.then === 'function') {
    // ...
    result.then((data) => console.log(data))
} else {
    // ...
    console.log("It´s a number", result)
}


function aNewPromise() {
    return new Promise((resolve, reject) => {
        resolve("aNewPromise")
    })
}

The problem: the ghost promise

Redefine 'job()' to avoid this conditional structure:

```
if (typeof result === 'object' && typeof result.then === 'function') {
    // ...
} else {
    // ...
}

It's important to reach the following output:

When 'value' is true:

```
aNewPromise
```

When 'value' is false:

```
It´s a number 42
```

*/


//+++ YOUR CODE GOES HERE


//aNewPromise()





//job()






//'value' to tests



//'result' to tests




// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*


//result promise
result

.then(data => console.log(data))


/* Output

- When 'value' is true:

aNewPromise

- When 'value' is false:

It´s a number 42

*/

















/*source:

- Adapted from https://www.codingame.com/playgrounds/347/javascript-promises-mastering-the-asynchronous/traps-of-promises


*/